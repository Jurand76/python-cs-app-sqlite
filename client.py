import socket as s
import json


class Client:
    HOST = "127.0.0.1"  # The server's hostname or IP address
    PORT = 60431  # The port used by the server

    def __init__(self):
        self.connection = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.connection.connect((self.HOST, self.PORT))

    def communication(self, query):
        data = query.encode('utf-8')
        self.connection.sendall(data)
        response = self.connection.recv(1024).decode('utf-8')
        unescaped_response = response.encode('utf-8').decode('unicode_escape')
        return unescaped_response


client = Client()

query = ""
while query != "stop":
    query = input("Input server query: ")
    if query != 'help':
        print(f"{client.communication(query)}")
    else:
        resp = client.communication(query)
        data = json.loads(resp)
        for key, value in data.items():
            print(key, ' - ', value)


