import sqlite3
from queue import Queue
import threading
from sqlite3 import Error


class ConnectionPool:

    def __init__(self, minconn, maxconn, SQLpath):
        self.minconn = minconn
        self.maxconn = maxconn
        self.currentconn = 0
        self.SQLpath = SQLpath
        self.pool = Queue()
        self.semaphore = threading.Semaphore(maxconn)
        self.lock = threading.Lock()
        self.initialize_pool()

    def initialize_pool(self):
        for _ in range(self.minconn):
            with self.lock:
                conn = self.create_connection()
                self.pool.put(conn)

    def create_connection(self):
        try:
            connection = sqlite3.connect(self.SQLpath, check_same_thread=False)
            if connection:
                return connection
        except Error as e:
            return None

    def get_conn(self):
        with self.semaphore:
            if self.pool.empty():
                if self.currentconn < self.maxconn:  # Jeśli można, stwórz nowe połączenie
                    self.currentconn += 1
                    return self.create_connection()
                else:
                    raise ValueError("Osiągnięto limit maksymalnej liczby połączeń")
            else:
                self.currentconn += 1
                return self.pool.get()

    def put_conn(self, conn):
        with self.lock:
            self.pool.put(conn)
            self.currentconn -= 1

    def connections_check(self):
        if self.currentconn > self.minconn:
            conn_to_delete = self.pool.get()
            conn_to_delete.close()
            self.currentconn -= 1
        if self.currentconn < self.minconn:
            self.pool.put(self.create_connection())
            self.currentconn += 1

    def close_all(self):
        while not self.pool.empty():
            conn = self.pool.get()
            conn.close()
        self.currentconn = 0
