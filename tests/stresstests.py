import database as db
import connectionpool as cp
import threading
import time

serv = db.DBServer()

def read_from_server(thread_id):
    try:
        answer = serv.db_read("SELECT * FROM users")
        print(f"Wątek nr : {thread_id}, {answer}")
    except ValueError as e:
        print(f'Numer wątku {thread_id}, błąd: {e}')


# -------------------------------- Sequential read -------------------------------
print('SEQUENTIAL READ')
start_time_1 = time.time()
for i in range(1000):
    read_from_server(1)
end_time_1 = time.time()


# -------------------------------- Multithread read -------------------------------
print()
print('MULTITHREAD READ')
start_time_2 = time.time()

threads = []
for i in range(1000):
    thread = threading.Thread(target=read_from_server, args=(i,))
    thread.start()
    threads.append(thread)

for thread in threads:
    thread.join()
end_time_2 = time.time()

print('Czas 1000 odczytów sekwencyjnych : ', end_time_1 - start_time_1, 'sek.')
print('Czas 1000 odczytów równoległych : ', end_time_2 - start_time_2, 'sek.')

serv.db_server_close()
