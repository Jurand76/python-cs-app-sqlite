import sqlite3
import threading
from sqlite3 import Error
import logindata
import connectionpool as cp


class DBServer:
    def __init__(self):
        self.SQLpath = r"d:\tomek\python\pycharm\python-cs-app-sqlite\pythonsqlite.db"
        self.conn_pool = cp.ConnectionPool(15, 40, self.SQLpath)


    def db_server_start(self):
        connection = sqlite3.connect(self.SQLpath)
        cursor = connection.cursor()

        print('Connected to SQLite3 database')
        print(sqlite3.version)

        # creating tables - users and messages
        try:
            cursor.execute('''
                CREATE TABLE IF NOT EXISTS users (
                    id integer PRIMARY KEY,
                    username text NOT NULL,
                    password text NOT NULL,
                    admin_privileges boolean NOT NULL DEFAULT FALSE
                );
            ''')
            connection.commit()
        except Error:
            print('Table users exists')
            connection.rollback()

        try:
            cursor.execute('''
                CREATE TABLE IF NOT EXISTS messages (
                    id integer PRIMARY KEY,
                    userfrom text NOT NULL,
                    userto text NOT NULL,
                    messagetext text NOT NULL,
                    read boolean NOT NULL DEFAULT FALSE
                );
            ''')
            connection.commit()
        except Error:
            print('Table messages exists')
            connection.rollback()

        cursor.close()
        self.conn_pool.put_conn(connection)

    def db_read(self, command):
        connection = None
        try:
            connection = self.conn_pool.get_conn()
            cursor = connection.cursor()
            cursor.execute(command)
            connection.commit()
            response = cursor.fetchall()
            cursor.close()
            self.conn_pool.put_conn(connection)
            return response

        except Error as e:
            print(f'Database reply - command "{command}" not executed')
            print(f'Error : {e}')
            if connection:
                connection.rollback()
                self.conn_pool.put_conn(connection)
            return "Error - not executed"


    def db_write(self, command):
        connection = self.conn_pool.get_conn()
        cursor = connection.cursor()
        try:
            cursor.execute(command)
            connection.commit()
            self.conn_pool.put_conn(connection)
            return "Executed"
        except Error:
            print(f'Database reply - command "{command}" not executed')
            connection.rollback()
            self.conn_pool.put_conn(connection)
            return "Not executed"


    def db_server_close(self):
        self.conn_pool.close_all()
        print('Server SQLite has been stopped')
