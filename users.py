import json

def load_users(server):
    try:
        result = {}
        command = "SELECT username, password, admin_privileges FROM users"
        response = server.db_read(command)
        print("Users list loaded")
        for row in response:
            result[row[0]] = {"password": row[1], "admin": row[2]}
        return result
    except FileNotFoundError:
        print('Users list empty')
        return ''


def user_list(users_list):
    response = "Users: "
    for item in users_list:
        response = response + item + ', '
    return response


def user_new(data, users_list, server):
    try:
        parts = data[8:].split(',')
        username = parts[0].replace(" ", "")
        password = parts[1].replace(" ", "")
        admin = parts[2].replace(" ", "").lower()

        if admin.lower() == "no" or admin.lower() == "false":
            admin_bool = False
        else:
            admin_bool = True

        print('New user data entered: ')
        print('Part 0 - username - : ', parts[0].replace(" ", ""))
        print('Part 1 - password - : ', parts[1].replace(" ", ""))
        print('Part 2 - is admin - : ', parts[2].replace(" ", ""))

        # Check if the user already exists
        if username in users_list:
            response = "Error: User already exists."
        else:
            users_list[username] = {'password': password, 'admin': admin}
            command = f"INSERT INTO users (username, password, admin_privileges) VALUES ('{username}', '{password}', {admin_bool})"
            print("Command: ", command)
            db_response = server.db_write(command)
            print("Command result: ", db_response)
            if db_response != "Not executed":
                response = "User created successfully."
            else:
                response = "User not created - database problem."
    except NotImplementedError:
        response = "Bad parameters of command - enter username, password, no/yes (admin privileges)"

    return response


def user_log(data, users_list):
    try:
        parts = data[8:].split(',')
        username = parts[0].replace(" ", "")
        password = parts[1].replace(" ", "")

        print('Logging user: ')
        print(f'Part 0 - username - : {parts[0].replace(" ", "")}')

        # Check if the user already exists
        if username in users_list:
            if users_list[username]['password'] == password:
                user_logged = username
                print(f'Username to log: {user_logged}')
                user_admin = users_list[username]['admin']
                print(f'Administrator: {user_admin}')
                response = user_logged
                print('Login successful')
            else:
                response = "Cannot log this user - bad password"
        else:
            response = f"Cannot log this user - not exists in database"
    except NotImplementedError:
        response = "Cannot log this user - bad parameters"

    return response


def user_delete(data, server, users_list, user_admin):
    try:
        parts = data[11:]
        username = parts.replace(' ', '')

        print('Deleting user: ')
        print('Part 0 - username - : ', username)

        # Check if the user already exists and if logged user has active admin privileges
        if username in users_list:
            if user_admin:
                del users_list[username]
                command = 'DELETE FROM users WHERE username = '
                command = command + "'" + username + "'"
                db_response = server.db_write(command)
                if db_response == "Executed":
                    response = f"Deleted user: {username}"
                    print('User delete successful')
                else:
                    response = f"Cannot delete user: {username}"
            else:
                response = "You need administrator privileges to delete users"
        else:
            response = f"User {username} not exists in database"
    except NotImplementedError:
        response = "Bad parameters of command"

    return response


def user_messages_load(username, server):
    if username == 'none':
        response = "No logged user"
        return response
    else:
        msg = {}
        try:
            command = "SELECT id, userfrom, userto, messagetext, read FROM messages WHERE userto = '" + username + "'"
            response = server.db_read(command)
            i = 1
            for row in response:
                print('Row: ', row)
                msg[i] = {"id": row[0], "from": row[1], "message": row[3], "read": row[4]}
                i += 1
            return msg
        except FileNotFoundError:
            print('No messages')
            msg = {}
    return msg


def user_messages(user_logged, messages):
    if user_logged == 'none':
        response = "No logged user"
    else:
        response = f"{len(messages)} messages for user: {user_logged}"
    return response
