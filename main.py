import database
from concurrent.futures import ThreadPoolExecutor

# Utwórz instancję DBServer
db_server = database.DBServer()


# Funkcja do wykonania zapytania; używa globalnej instancji db_server
def execute_query(query):
    result = db_server.db_read(query)
    return result


# Uruchom 10 równoległych zapytań
with ThreadPoolExecutor(max_workers=10) as executor:
    queries = ["SELECT * FROM users" for _ in range(10)]
    results = list(executor.map(execute_query, queries))

# Wyświetl wyniki
for result in results:
    print(result)
